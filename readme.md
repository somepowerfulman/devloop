# Answers for questions app
Shows list of qeustions and allows to answer them

in catatog postman queries exist postman collections

##  Installation Using Docker

### 1. Setup .env file
```
cp .env.example .env
```

### 2. Build application
```
docker-compose build
```

### 3. install composer
```
docker-compose run --rm app sh -c 'composer install'
```

### 4. Run migrations
```
docker-compose run --rm app sh -c 'php artisan migrate'
```
### 5. Run seed
```
docker-compose run --rm app sh -c 'php artisan db:seed'
```

### 6. Run application

```
docker-compose up -d
```

##access service on url: http://localhost
1. login and get <token> from response POST api/login
2. get list of questions GET /api/questions?token=<token>
3. create an answer for question POST  /api/questions/2/answers


TODO:
add tests
fix styling
clean up dependencies in dockerfile
