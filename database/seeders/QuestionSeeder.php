<?php

namespace Database\Seeders;

use App\Models\Question;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::create([

            'question' => 'first question',
            'order' => 1,
        ]);
        Question::create([

            'question' => 'second question',
            'order' => 1,
            'parent_id' => 1

        ]);
        Question::create([

            'question' => 'third question',
            'order' => 1,

        ]);

    }
}
