<?php

namespace Database\Seeders;

use App\Models\AnswerVariant;
use Illuminate\Database\Seeder;

class AnswerVariantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AnswerVariant::create([

            'answer' => 'first answer',
            'question_id' => 1,
        ]);
        AnswerVariant::create([

            'answer' => 'second answer',
            'question_id' => 1,
        ]);
        AnswerVariant::create([

            'answer' => 'third answer',
            'question_id' => 2,

        ]);
    }
}
