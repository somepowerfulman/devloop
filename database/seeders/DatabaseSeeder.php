<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         Question::factory(10)->create();
        $this->call([
            QuestionSeeder::class,
            UserSeeder::class,
            AnswerVariantSeeder::class
            ]);

    }
}

