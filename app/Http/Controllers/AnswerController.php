<?php

namespace App\Http\Controllers;

use App\Http\Requests\AnswerRequest;
use App\Http\Resources\AnswerResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnswerController extends Controller
{

    /**
     * @param Request $request
     * @param int $question_id
     * @return mixed
     */

    public function store(AnswerRequest $request, int $question_id)
    {
        $answers = $request->validated()['answers'];
        $user = Auth::user();
        $responce = $user->answers()->createMany($answers);
        return AnswerResource::collection($responce)->response()->setStatusCode(201);
    }
}
