<?php

namespace App\Http\Controllers;

use App\Http\Resources\QuestionResource;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{


    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $questions = Question::with(['subQuestions'])
            ->where('parent_id', '=', null)
            ->skip($request->query('skip', 0))
            ->take($request->query('take', 5))->get();
        return QuestionResource::collection($questions);
    }


    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id)
    {
        $question = Question::findOrFail($id);
        return new QuestionResource($question);
    }

}
