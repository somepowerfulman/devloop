<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Answer
 * @package App\Models
 *
 * @property-read int id
 * @property int question_id
 * @property int user_id
 * @property int variant_id
 * @property-read Carbon created_at
 * @property-read Carbon updated_at
 */
class Answer extends Model
{
    use HasFactory;
    protected $fillable = ['question_id', 'user_id', 'variant_id'];
}
