<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Collection;

/**
 * Class Question
 * @package App\Models
 *
 * @property-read  int id
 * @property  string question
 * @property  int order
 * @property  int parent_id
 * @property-read  Carbon created_at
 * @property-read  Carbon updated_at
 *
 * @property-read  Collection answerVariants
 * @property-read  Collection subQuestions
 *
 */
class Question extends Model
{
    protected $fillable = ['question', 'order', 'parent_id'];
    protected $with = ['answerVariants'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function answerVariants()
    {
        return $this->hasMany(AnswerVariant::class, 'question_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subQuestions()
    {
        return $this->hasMany(Question::class, 'parent_id', 'id');
    }
}
