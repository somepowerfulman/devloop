<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Collection\Collection;

/**
 * Class AnswerVariant
 * @package App\Models
 *
 * @property-read int id
 * @property string answer
 * @property int question_id
 * @property-read Carbon created_at
 * @property-read Carbon updated_at
 *
 * @property-read Collection question
 *
 */
class AnswerVariant extends Model
{
    protected $fillable = ['answer', 'question_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(Question::class, 'id', 'question_id');
    }

}
